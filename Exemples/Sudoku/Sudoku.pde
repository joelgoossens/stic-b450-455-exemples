// Auteur Joël Goossens, Université libre de Bruxelles
// Une partie de la solution est fondée sur le code de 
// Ernest Warzocha sur https://openprocessing.org/sketch/877170
// Janvier 2024, Bruxelles

Block[][] blocks; // La grille est une matrice d'ojets Block

int totalFilledNumbers = 21; //Nombre initial de valeurs dans la grille

void setup(){
    size(600,600);
    fillGrid();
    thread("Anime"); // Appel solveSudoku(blocks, 0, 0);
}

void draw(){
    background(0);
    drawGrid();
}

boolean solveSudoku(Block B[][], int x, int y){
    // une solution est alors trouvée
    if (x == 8 && y == 9)
        return true;
 
    // Check if y value  becomes 9 , 
    // we move to next x and
    //  yumn start from 0
    // Si l'on atteint la ligne 9, il faut passer à la colonne
    // suivante, première ligne
    if (y == 9) {
        x++;
        y = 0;
    }
    // Si la valeur est constante on avance dans la recherche
    if (B[x][y].isConstant)
        return solveSudoku(B, x, y + 1); //Appel récursif
    
    // On teste potentiellement les 9 valeurs possibles
    for (int num = 1; num <= 9; num++) {
        // s contient l'élément courant de la grille
        Block s = B[x][y];
        // Si la valeur est admissible selon les règles du Sudoku
        if (s.check(num)){
            // On tente cette valeur
            s.number = num;
            s.isFilled = true;
            s.isValid = true;
            s.isConstant = false;
           
            //  Checking for next possibility with next
            //  column
            int d = int(constrain(mouseX, 0, 500));
            delay(d);

            // On avance dans la recherche en considérant la case suivante
            // Appel récursif
            if (solveSudoku(B, x, y + 1)) //Appel récursif
                return true;
        }
    }
    // À ce stade toutes les valeurs ont échouées
    B[x][y].isFilled = false;
    return false; // L'appel se termine et retourne faux
}

void Anime(){
    Fin = false;
    Res = solveSudoku(blocks, 0, 0);
    Fin = true;
}

void mousePressed(){
    if (mouseButton == RIGHT)
        setup();
}
