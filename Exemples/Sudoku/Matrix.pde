int border=100;
boolean Res, Fin=false;
int blockSize;

void fillGrid(){
    textAlign(CENTER, CENTER);
  textSize(36);
  
  blockSize = (width-border)/9;
  blocks = new Block[9][9];
  
  for (int i = 0; i<9; i++){
    for(int j = 0; j<9; j++){
      blocks[i][j] = new Block(i,j);
    }
  }
  
  int countStartingBlocks = 0;

// Remplissage aléatoire de la grille avec totalFilledNumbers nombres au départ
  while(countStartingBlocks < totalFilledNumbers){
    int randX = int(min(random(9),8));
    int randY = int(min(random(9),8));
    
    Block s = blocks[randX][randY];
    
    if(!s.isFilled){

      int number = int(min(random(1,10),9));
      boolean checkNumber = s.check(number);
      
      if(checkNumber) {
        s.isFilled = true;
        s.isConstant = true;
        s.isValid = true;
        s.number = number;
        countStartingBlocks++;
      }
    }
  }
}

void drawGrid(){
  for (int i = 0; i<9; i++){
    for(int j = 0; j<9; j++){
        textSize(32);
      blocks[i][j].draw();
    }
  }

  strokeWeight(4);
  line(3*blockSize,0, 3*blockSize, height-border);
  line(6*blockSize,0, 6*blockSize, height-border);
  line(0, 3*blockSize, width-border, 3*blockSize);
  line(0, 6*blockSize, width-border, 6*blockSize);
  noFill();
  rect(0,0,width-border,height-border);
  fill(255);
  textSize(12);
    if (Fin)
      if (Res){
        
        text("OK", 100, height-border/2);
      }
      else {
        
        text("KO", 100, height-border/2);
      }
    else{
        
        text("In progress...", 100, height-border/2);
      }
      text("right-click to restart", 300, height-border/2);
      text("Move the mouse to the left to speedup", 300, height-border/2+20);
    
}
