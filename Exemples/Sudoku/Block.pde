class Block {
    int x;
    int y;
    int number;
    boolean isFilled;
    boolean isConstant = false;
    boolean isActive = false;
    boolean isValid = false;

  Block(int a, int b){ // le constructeur
    x = a;
    y = b;
    isFilled = false;
  }

// Vérifie si la valeur a peut être place à cette position
    boolean check(int a){
    //On vérifie la colonne
    for(int row = 0; row<9;row++)
      if(row!=x && blocks[row][y].isFilled)
        if(blocks[row][y].number == a)
          return false;
    // On vérifie la ligne
    for(int col = 0; col<9;col++)
        if(col!=y && blocks[x][col].isFilled)
          if(blocks[x][col].number == a) 
              return false;  
    
    int largeSquareX = floor(x/3);
    int largeSquareY = floor(y/3);
    
    int largeSquareCenterX = (largeSquareX*3)+1;
    int largeSquareCenterY = (largeSquareY*3)+1;
    
    // On vérifie le carré
    for(int i = -1; i<=1; i++)
      for(int j = -1; j<=1; j++)
          if((i+largeSquareCenterX != x || j+largeSquareCenterY != y) && blocks[i+largeSquareCenterX][j+largeSquareCenterY].isFilled)
            if(blocks[i+largeSquareCenterX][j+largeSquareCenterY].number == a)
               return false;
    return true;
  }
  
  // Fonction qui permet de dessiner un élément de la grille
    void draw(){
    color col;
    col = color(200,255,200);
    if(!isFilled) col = (255);
	  if(isConstant) col = color(200,200,200);
		
    strokeWeight(1);
    fill(col);
    rect(x*blockSize,y*blockSize,blockSize,blockSize);  
    if(isFilled){
      if(isConstant) fill(0);
      else fill(100);
      text(number, x*blockSize+blockSize/2, y*blockSize+blockSize/2);
    }
  }
}
