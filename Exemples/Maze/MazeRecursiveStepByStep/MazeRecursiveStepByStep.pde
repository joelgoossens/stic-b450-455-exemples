/*

Algorithme récursif à la recherche de la sortie du labyrinthe

Auteurs : Julien Jabon & Joël Goossens (ULB – Univeristé libre de Bruxelles)

Février 2017

Code couleur et forme.
   => Un carré rose, est l'algorithme qui avance et explore.
   => Un rond  rouge, il y a dead End
   => fle
   flèche, on a découvert la porte de sortie.
*/

/**************GLOBAL VARIABLE*******************/
MazeGenerator maze; // Variable contenant le labyrinthe
int maze_size = 9; // Nombre de tile par hauteur et longeur
int screen_size = 800;                   // Taille de l'écran
int case_size = screen_size/maze_size;    // Rapport nombre de cases
int state = 0;                            // État pour la gestion des threads
Perso my_c;
boolean[][] wasHere; // Matrice pour marquer le passage
int[][] correctPath; // Matrice pour le chemin vers la sortie

//Valeur possible pour correctPath
final int unvisited = -1;   // on n'est pas passé par là
final int ok = 1;           // la case mène à la sortie
final int deadend = 10;     // la case mène à un cul de sac
final int already = 3;      // on est déja passé par là

/***********SETUP***********************/
void setup(){
  frameRate(60);     //nombre de frame par seconde
  imageMode(CENTER);
  size(800, 800);    // taille de la fenetre
  my_c = new Perso();

  maze = new MazeGenerator(maze_size); // Creation et génération du labyrinthe

  //creation des matrices wasHere et correctPath.
  wasHere = new boolean[ maze.Size()][ maze.Size()];
  correctPath = new int[maze.Size()][maze.Size()];

  maze.ASCIIdisplay();   //Affichage du labyrinthe sur la console.
  maze.display();        //On dessine le labyrinthe à l'écran
  maze.ShowRecursiveAlgo("SolvingMaze"); // anime et exécute la fonction SolvingMaze
}

/********************DRAW*******************/
void draw(){
  background(0);     //On dessine le background
  maze.display();    //On dessine le labyrinthe
  displaywasHerecorrectPath();
}

public void displaywasHerecorrectPath (){
  //On dessine les endroits visités et les endroits qui mènent à la sortie
  for (int i =0; i < maze.Size() ; i++)
    for (int j =0; j < maze.Size() ; j++){
      fill(234,22,222);
      if (wasHere[i][j])
        rect(i*case_size+case_size/2, j*case_size+case_size/2,4,4 );
        // on dessine un petit carré

      //si on a le correct path entre 0 et 4, on dessine le personnage en mode ghost.
      if (correctPath[i][j] >= 0 && correctPath[i][j] <= 4) {
            Perso p = new Perso(correctPath[i][j],i,j);
            p.drawMeAsGhost();
      }

      fill(255,0,0);
      if(correctPath[i][j] == deadend)
        ellipse(i*case_size+case_size/2,j*case_size+case_size/2,10,10 );
        // on dessine les ronds rouges
     }
}

// Initialisation de l'algorithme.
public void SolvingMaze(){
  println("start recursive");
  // Initialisation
  for (int row = 0; row < maze.Size(); row++)
    // Sets boolean Arrays to default values
    for (int col = 0; col < maze.Size(); col++){
      wasHere[row][col] = false;
      correctPath[row][col] = unvisited;
    }

    //On lance l'algorithme avec les coordonnées de départ du personnage.
    recursiveSolve(maze.startX(), maze.startY());
}

//======================================

public int recursiveSolve(int x, int y){

  maze.WaitAnimation(100);
  if(maze.endX() == x && maze.endY() == y){
  // On regarde si on est pas sur la fin.
    wasHere[x][y] = true;
    return ok;  //si oui, on l'annonce.
  }
  if(wasHere[x][y])
    return already;  // on regarde si on est pas déja sur une case déjà visité
  wasHere[x][y] = true;  //On marque la case comme quoi on est déja passé.
  if(x !=0 && y!=0 && maze.isDeadEnd(x,y)){
  // Si c'est un Cul de sac.
    correctPath[x][y] = deadend;     //On marque que c'est un deadend
    return deadend;             //On retourne le code deadend
    }
  println("R",x,y,":",maze.isNorthWall(x,y),maze.isSouthWall(x,y),maze.isEastWall(x,y),maze.isWestWall(x,y));

  //On poursuit la recherche dans les quatres directions
  int[] ret = new int[4]; // on va stocker les 4 résultats
  if(!maze.isNorthWall(x,y))  ret[0]=recursiveSolve(x,y-1);
  if(!maze.isEastWall(x,y))   ret[1]=recursiveSolve(x+1,y);
  if(!maze.isSouthWall(x,y))  ret[2]=recursiveSolve(x,y+1);
  if(!maze.isWestWall(x,y))   ret[3]=recursiveSolve(x-1,y);

  //On regarde si aucun ne mène vers la sortie
  if(ret[0] != ok && ret[1] != ok && ret[2] != ok && ret[3] != ok)
    {// toutes sont soit des deadend.
      //si non, on marque et on retourne.
      correctPath[x][y] = deadend;
      return deadend;
    }
    //On cherche quelle direction mène vers la sortie
    if(ret[0] == ok) correctPath[x][y] = 0;
    if(ret[1] == ok) correctPath[x][y] = 1;
    if(ret[2] == ok) correctPath[x][y] = 2;
    if(ret[3] == ok) correctPath[x][y] = 3;
    return ok;
 }
