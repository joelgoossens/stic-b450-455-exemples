

public class Perso
{
int x;
int y;
int orientation;
PImage[] sprite;

  public void init()
  {
    x = 0;
    y = 0;
    orientation = 2;
    
    sprite = new PImage[4];
    sprite[3] =loadImage("Left.png");
    sprite[1] =loadImage("Right.png");
    sprite[2] =loadImage("Front.png");
    sprite[0] =loadImage("Back.png");

  }

  public Perso(int o, int _x,int _y)
  {
   init();
   orientation = o;
   x = _x;
   y =_y;
  }
  public Perso()
  {
   init();
  }
public int getX(){return x;}
public int getY(){return y;}

  
public boolean isAWallOnRigthHand()
{
  println("isAWallOnRigthHand]]::",orientation, x, y);
  if(orientation%4 == 0)return maze.isEastWall( x, y);
  if(orientation%4 == 1)return maze.isSouthWall(x, y);
  if(orientation%4 == 2)return maze.isWestWall( x, y);
  if(orientation%4 == 3)return maze.isNorthWall(x, y);  
  return false;
}

public boolean isWallAhead()
{
  println("isWallAhead]]::",orientation,  x, y);
  if(orientation%4 == 0)return maze.isNorthWall( x, y);
  if(orientation%4 == 1)return maze.isEastWall(  x, y);
  if(orientation%4 == 2)return maze.isSouthWall( x, y);
  if(orientation%4 == 3)return maze.isWestWall(  x, y);  

  return false;
}


public int  getOrientation(){return orientation;}

public void turnLeft()
{
  orientation+=3;
  println("turnLeft]] new orientation",orientation);
}

public void turnRight()
{
  orientation++;
  println("turnRight]] new orientation",orientation);
}

public void OneStep()
{
  if(orientation%4 == 0)y--;
  if(orientation%4 == 1)x++;
  if(orientation%4 == 2)y++;
  if(orientation%4 == 3)x--;
}
void display()
{
  display(255);
}

void display(int t)
  {
      tint(255, t); 
      fill(232,223,123);
      image(sprite[orientation%4],x*case_size+case_size/2,y*case_size+case_size/2, case_size/2, case_size/2);
 
  }

void drawMeAsGhost()
  {
  display(120); 
  }


}