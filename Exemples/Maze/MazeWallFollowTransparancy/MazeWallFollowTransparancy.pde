/*
Programme qui montre l'algorithme handfollow.
Le personnage est a moitié transparent pour montrer qu'il repasse par plusieurs endroits.
*/

/*****************************GLOBAL VARIABLE***************************************/
MazeGenerator maze;                        // Variable contenant le labyrinthe
Perso p;                                   // Variable contenant le personnage
int maze_size = 25;                        // Nombre de tile par hauteur et longeur
int screen_size = 1000;                    // Taille de l'écran
int case_size = screen_size/maze_size;     // Rapport nombre de tile/ ecran
int stateturn = 0;                         // Machine à état.


/*****************************SETUP***************************************/
void setup()
{
  frameRate(60);                          //nombre de frame par seconde
  size(1000, 1000);                       // taille de la fenetre
  maze = new MazeGenerator(maze_size);    // Creation et génération du maze  (maze carré).
  maze.ASCIIdisplay();                    //Affichage du labyrinthe sur la console.
  p = new Perso();                        //Creation du personnage (param
  maze.display();                       //On dessine le labyrinthe à l'écran
}

/*****************************DRAW***************************************/
void draw(){ 
  p.display(90);                          //On dessine le personnage 
  handFollow();
}


/*****************************KEY PRESSED***************************************/
// La machine a état de 4 état. 
// La raison est du quand on tombe sur une case sans mur sur la main Droite
//  
//+---+---+---+
//|        => |
//+---+---+   +
//|   |       |
//+   +   +---+
//
//+---+---+---+
//|        => |
//+---+---+   +
//|       |   |
// Dans ce cas là, il faut retrouver le mur en tournant.
/********************************************************************************/
void handFollow() {
  
 if (p.getX() == maze.endX() && p.getY() == maze.endY()) return;      // Si on a trouvé la fin, on ne fait plus rien. 

 if(stateturn == 0)                                                   // State 0 : le cas le plus simple. Peut se résoudre en une étape.            
 {
     if(p.isAWallOnRigthHand() && !p.isWallAhead())                   // Le personnage voit un mur à droite et pas de mur devant
      {
        println("no WallAhead and WallOnRigthHand");
        p.OneStep();                                                 // Le personnage avance d'un pas
                                                                     // on reste en state 0           
      }else if (p.isAWallOnRigthHand() && p.isWallAhead() )          // un mur a droite et un mur devant 
      {
        println("WallAhead and WallOnRigthHand");
        p.turnLeft();                                               // Le personnage tourne seulement d'un cran à gauche.
                                                                    // On reste en state O
      }else if (!p.isAWallOnRigthHand())                            // Le personnage n'a plus de mur à droite
      { 
        println("Oh my god no walls anymore");                      
        p.turnRight();                                              // Il tourne a droite et paste la machine à état en state 1
        stateturn = 1;          
      }
  }else if (stateturn == 1)                                         // state 1 : il n'ya plus de mur sous la main.
  {
   p.OneStep();                                                     // Le personnage avance d'un cran
   if (!p.isAWallOnRigthHand())                                     // Le personnage regarde si il y a un mur
       {
         stateturn = 2;                                             //si pas de mur on continue a tourner...
       }
   else 
     {
       stateturn = 0;                                               // Si mur, on revient en state 0.
     }
  }else if (stateturn == 2)
        {
          p.turnRight();                                            //  Toujours pas de mur, le personnage tourne et passe en étape 3.
          stateturn = 3;
  }else if (stateturn == 3)
  {
          p.OneStep();                                             // La il y a obligatoirement un mur. 
          stateturn = 0;
  }
}