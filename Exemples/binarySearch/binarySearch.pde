/*
   Recherche par dichotomie
*/

int num_items = 20;
int minval = 1;
int maxval = 30;
int inf, sup, ptr, searchValue;
boolean trouve;

int[] unsorted;
int[] bubblesorted;

void setup() {
    size(450, 400);
    colorMode(HSB,360);
    textFont(createFont("SansSerif", 12, true));
    noStroke();

    frameRate(1);
    searchValue = (int) random(minval, maxval);

    unsorted = new int[num_items];
    for (int i=0; i<num_items; i++) {
        unsorted[i] = (int) random(minval, maxval);
    }

    bubblesorted = new int[num_items];
    arrayCopy(unsorted, bubblesorted);
    bubbleSort(bubblesorted);
    ptr=0;
    inf=0; 
    sup=bubblesorted.length-1;
    trouve=false;
    //noLoop();
}

void bubbleSort(int A[]){
  int t, droite, i;
  for(droite=A.length-1;droite>0;--droite)
    for(i=0;i<droite;++i)
      if (A[i]>A[i+1]){
      t=A[i];A[i]=A[i+1];A[i+1]=t;   
    }  
}
void draw() {
    fill(360, 200);
    rect(0, 0, width, height);

    fill(0);
    textSize(12);
    text("Right-click to generate a new random array.", 30,370);

    //visualize("Unsorted",      unsorted,       30,50,  20, false);
    visualize("sorted",bubblesorted,30,150, 20, true);
    //visualize("Selection sort",   selectionSorted, 30,150, 20, true);
    //selectionMax_one_step();
    //selectionSort_one_step();
    //noLoop();
    binarySearch_one_step();
}

void binarySearch_one_step(){
  if (inf > sup || trouve)
    return; // We are done
  ptr = (inf + sup)/2;
  println (ptr);
  if (bubblesorted[ptr]==searchValue)
    trouve=true;
  else
    if (bubblesorted[ptr]>searchValue) //ou A[I[ptr].ref].nom>x
      sup = ptr-1;
    else
      inf = ptr+1;
}
void mousePressed() {
    if (mouseButton == RIGHT)
        setup();
}

void visualize(String caption,
               int[] values,
               float x, float y, float xstep,
               boolean showCursor) {
    fill(0);
    textSize(14);
    text(caption, x, y);
    text(searchValue, x+100, y);
    textSize(10);
    for (int i=0; i < values.length; i++) {
        int v = values[i];
        if ((i == inf || i==sup) && showCursor) {
            fill(0);
            noStroke();
            rect(x+(xstep*i), y+3, 5, 5);
        }
        if (i == ptr && showCursor) {
            fill(0);
            noStroke();
            triangle(x+(xstep*i)+2.5,y+15,x+(xstep*i),y+10,x+(xstep*i+5),y+10);
        }
        
        fill(0);
        text(str(v), x+(xstep*i), y+25);

        float w = map(v, minval, maxval, 3, 15);
        float h = map(v, minval, maxval, 10, 50);

        fill( valueToColor(v) );
        rect(x+(xstep*i), y+30, w, h);
    }
    if (trouve && showCursor){
          fill(0);
          textSize(14);
          text("élément trouvé", x+150, y);
        }
        else if(inf>sup && showCursor){
          fill(0);
          textSize(14);
          text("élément non trouvé", x+150, y);
        }
}

// Convert a  [minval, maxval] value to a color
int valueToColor(int value) {
    float hue = norm(value, minval, maxval) * 360;
    return color(hue, 180, 360);
}

int mybinarySearch(int A[], int searchValue){
  int inf=0,sup=A.length-1,ptr=0;
  boolean trouve=false;

  while (sup >= inf && !trouve){
    ptr = (inf + sup)/2;
    if (A[ptr]==searchValue)
      trouve=true;
    else if (A[ptr]>searchValue) 
      sup = ptr-1;
    else
      inf = ptr+1;
  }
  if (trouve)
    return ptr; 
  else
    return -1;
}