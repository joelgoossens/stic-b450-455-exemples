/*
   QuickSort
*/

int num_items = 20;
int minval = 1;
int maxval = 99;
int visitor, tail;
int posMax;

int[] unsorted;
int[] QuickSorted;

void setup() {
    int res, searchValue;
    size(450, 400);
    colorMode(HSB,360);
    textFont(createFont("SansSerif", 12, true));
    noStroke();

    frameRate(12);
    tail = num_items-1;

    unsorted = new int[num_items];
    for (int i=0; i<num_items; i++) {
        unsorted[i] = (int) random(minval, maxval);
    }

    QuickSorted = new int[num_items];
    arrayCopy(unsorted, QuickSorted);
    QuickSort_one_pass(0,num_items-1);
    searchValue = (int) random(minval, maxval);
    res = binary_search(searchValue,0,num_items-1);
    if (res == -1)
        println ("La valeur recherchée", searchValue, "ne se trouve pas dans le tableau");
    else
        println ("La valeur recherchée", searchValue, "se trouve pas dans le tableau en position", res);
}

void draw() {
    fill(360, 200);
    rect(0, 0, width, height);

    fill(0);
    textSize(12);
    text("Right-click to generate a new random array.", 30,370);

    //visualize("Unsorted",      unsorted,       30,50,  20, false);
    visualize("QuickSort",QuickSorted,30,150, 20,false);
}

void mousePressed() {
    if (mouseButton == RIGHT)
        setup();
}

int binary_search(int toFind, int start, int end){
    int mid = (start + end)/2;   //Integer division
    //Stop condition.
    println("appel binary_search(", toFind, start, end, ")");
    if (start > end)
       return -1;
    else if (QuickSorted[mid] == toFind) 
       return mid;
    else if (QuickSorted[mid] > toFind)  
       return binary_search(toFind, start, mid-1);
    else                          
       return binary_search(toFind, mid+1, end);
}

int partition(int lo, int hi){
    int pivot, tmp,i,j;
    pivot = QuickSorted[hi];
    i = lo;
    for (j=lo; j<hi; j++){
        if (QuickSorted[j] < pivot) {
            tmp = QuickSorted[i];
            QuickSorted[i] = QuickSorted[j];
            QuickSorted[j] = tmp;
            i++;
        }
    }
    tmp = QuickSorted[i];
    QuickSorted[i] = QuickSorted[hi];
    QuickSorted[hi] = tmp;
    return i;
}

void QuickSort_one_pass(int lo, int hi){
    int p;
    if (lo < hi){
        p = partition(lo, hi);
        QuickSort_one_pass(lo, p-1);
        QuickSort_one_pass(p + 1, hi);
    }
}

void visualize(String caption,
               int[] values,
               float x, float y, float xstep,
               boolean showCursor) {
    fill(0);
    textSize(14);
    text(caption, x, y);

    textSize(10);
    for (int i=0; i < values.length; i++) {
        int v = values[i];

        if (i == visitor && showCursor) {
            fill(0);
            noStroke();
            rect(x+(xstep*i), y+3, 5, 5);
        }


        fill(0);
        text(str(v), x+(xstep*i), y+25);

        float w = map(v, minval, maxval, 3, 15);
        float h = map(v, minval, maxval, 10, 50);

        fill( valueToColor(v) );
        rect(x+(xstep*i), y+30, w, h);
    }
}

// Convert a  [minval, maxval] value to a color
int valueToColor(int value) {
    float hue = norm(value, minval, maxval) * 360;
    return color(hue, 180, 360);
}
