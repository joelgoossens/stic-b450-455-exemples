void setup() {
  Table table = loadTable("data.csv","header");
  
  println("Unsorted version");
  printTable(table);
  table.sort("name");
  println("Sorted version");
  printTable(table);
  saveTable(table,"sorteddata.csv");
}

void printTable(Table T){
  for (int i=0; i<T.getRowCount();++i){
        TableRow row = T.getRow(i);
        int x = row.getInt("x");
        int y = row.getInt("y");
        float d = row.getFloat("diameter");
        String s = row.getString("name");
        println (x,y,d,s);
    }
}