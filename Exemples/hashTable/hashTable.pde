/*

Table de hachage
Auteur : Joël Goossens
Février 2017

*/

final int B=128;    // paramètre pour la hachage
final int N=10;     // taille du tableau sans collision
final int NMAX=15;  // taille supplémentaire pour les collisions
String[] nom = new String [NMAX];
String[] tel= new String [NMAX];
int[] col = new int [NMAX];
int nNoms; // indice actuel dans la zone de collision

void initialisation(){
  for (int i=0;i<NMAX;++i){
    nom[i]="";
    col[i]=-1;
    tel[i]="";
  }
  nNoms=N;
}

void affichage(){
  for (int i=0;i<NMAX;++i)
      println("[",i,"]",nom[i],tel[i],col[i]);
}

// Fonction de hachage (à partir d'une chaîne vers entier entre 0 et N-1)
int h (String x){
  int i, r;
  r = 0;
  for (i=0;i<x.length();++i)
    r = ((r * B) + (x.charAt(i))) % N;
  return r;
}

void insertion (String x, String val){
  int i;
  i = h(x); // on hache x
  if (nom[i].equals("")){ // l'emplacement est libre, on insère
    nom[i] = x;
    tel[i] = val;
  }
  else if (nNoms >= NMAX)
        println("insertion impossible"); //il n'y a plus de place dans la zone de collision
    else{ // on insère dans la zone de collision
        nom[nNoms] = x;
        tel[nNoms] = val;
        col[nNoms] = col[i];
        col[i] = nNoms;
        nNoms = nNoms + 1;
      }
}

int recherche (String x){
    int i;
    i = h(x); // on hache x
    while (!nom[i].equals(x) && col[i] != -1) //on parcours
      i = col[i];
    if (x == nom[i]) // on a trouvé
        return i;
    else
        return -1; // la valeur recherchée n'est pas dans la table
}

void setup (){

  initialisation();
  insertion("Jacques","5588");
  insertion("Roger","5500");
  insertion("Davis","1122");
  insertion("Lola","1775");
  insertion("Robert","4545");
  insertion("Emile","2456");
  affichage();

  println("tel d'Emile ", tel[recherche("Emile")]);
  println("position de Zoé ", recherche("Zoé"));
}
