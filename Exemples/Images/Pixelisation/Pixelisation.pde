PImage img;

void setup(){
    size(500,500);
    img = loadImage("CodingBildbis.jpg");
}

void draw(){
    //loadPixels();
    img.loadPixels();

    for (int y = 0; y < img.height; y += 10) {
        for (int x = 0; x < img.width; x += 10) {
            
            int loc = x+y*img.width;
            
            float r = red(img.pixels[loc]);
            float g = green(img.pixels[loc]);
            float b = blue(img.pixels[loc]);
            
            //fill(255);
            //text(pixel, x, y);
    
            
            fill(r, g, b, 100);
            ellipse (x, y, 10, 10);
        }
    }
}
