PImage img;

void setup(){
    size(500,500);
    img = loadImage("CodingBildbis.jpg");
}

void draw(){
    loadPixels();
    img.loadPixels();

    for (int y = 0; y < img.height; y += 1) {
        for (int x = 0; x < img.width; x += 1) {
            
            int loc = x+y*img.width;
            
            float r = red(img.pixels[loc]);
            float g = green(img.pixels[loc]);
            float b = blue(img.pixels[loc]);
            
            float tr = constrain(0.393*r + 0.769*g + 0.189*b,0,255);
            float tg = constrain(0.349*r + 0.686*g + 0.168*b,0,255);
            float tb = constrain(0.272*r + 0.534*g + 0.131*b,0,255);
            
           
            pixels[loc] = color(tr,tg,tb);  
          
        }
    }
    updatePixels();
}
