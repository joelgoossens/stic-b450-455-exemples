// Recherche Séquentielle

int num_items = 20;
int minval = 1;
int maxval = 25;
int visitor, tail;
int searchValue;
boolean trouve;

int[] unsorted;

void setup() {
    size(450, 400);
    colorMode(HSB,360);
    textFont(createFont("SansSerif", 12, true));
    noStroke();

    frameRate(2);
    tail = num_items-1;
    searchValue = (int) random(minval, maxval);

    unsorted = new int[num_items];
    for (int i=0; i<num_items; i++) {
        unsorted[i] = (int) random(minval, maxval);
    }

    visitor = 0;
    trouve=false;
}

void draw() {
    fill(360, 200);
    rect(0, 0, width, height);
    fill(0);
    textSize(12);
    text("Right-click to generate a new random array.", 30,370);
    visualize("Search",  unsorted,    30,150, 20, true);
    Search_one_step();
}

void mousePressed() {
    if (mouseButton == RIGHT)
        setup();
}

void Search_one_step() {
    if (visitor == num_items || trouve)
      return; // We are done
    if (unsorted[visitor]==searchValue){
      trouve = true;
    }
    else
     visitor++;
}

void visualize(String caption,
               int[] values,
               float x, float y, float xstep,
               boolean showCursor) {
    fill(0);
    textSize(14);
    text(caption, x, y);
    text(searchValue, x+100, y);
    textSize(10);

    for (int i=0; i < values.length; i++) {
        int v = values[i];

        if (i == visitor && showCursor) {
            fill(0);
            noStroke();
            rect(x+(xstep*i), y+3, 5, 5);
        }


        fill(0);
        text(str(v), x+(xstep*i), y+25);

        float w = map(v, minval, maxval, 3, 15);
        float h = map(v, minval, maxval, 10, 50);

        fill(valueToColor(v));
        rect(x+(xstep*i), y+30, w, h);
    }
    if (trouve){
          fill(0);
          textSize(14);
          text("élément trouvé", x+150, y);
        }
        else if(visitor==num_items){
          fill(0);
          textSize(14);
          text("élément non trouvé", x+150, y);
        }
}

// Convert a  [minval, maxval] value to a color
int valueToColor(int value) {
    float hue = norm(value, minval, maxval) * 360;
    return color(hue, 180, 360);
}

int sequentialSearch(int A[], int searchValue){
int i=0;
trouve=false;

while (i<A.length && !trouve){
  if (A[i]==searchValue)
    trouve=true;
  else
    ++i;
}
if (trouve)
    return i;
else
    return -1;
}
